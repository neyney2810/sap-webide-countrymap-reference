sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"Z03_CountryList/model/formatter"
], function(Controller, formatter) {
	"use strict";

	return Controller.extend("Z03_CountryList.controller.MasterView", {

		formatter: formatter,
		
		onInit: function () {
		
		},

		onListPressed: function(oEvent) {
			var sPath = oEvent.getSource()
				.getSelectedItem()
				.getBindingContext("countryModel")
				.getPath();
			var aContext = sPath.split("/");

			this.getOwnerComponent()
				.getRouter()
				.navTo("detail", {
					context: aContext[1],
					index: aContext[2]
				});

		}

	});

});